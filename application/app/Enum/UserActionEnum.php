<?php

namespace App\Enum;

class UserActionEnum
{
    public const ADD_USER_ACTION = 'addUser';
    public const GET_USER_ACTION = 'getUser';
    public const GET_USERS_ACTION = 'getUsers';
}

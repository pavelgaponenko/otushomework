<?php

namespace App\Enum;

class StatusEnum
{
    public const PROCESS_STATUS = 0;
    public const COMPLETE_STATUS = 1;
    public const FAILED_STATUS = 2;
}

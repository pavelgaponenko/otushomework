sudo cp deploy/nginx.conf /etc/nginx/conf.d/demo.conf -f
sudo sed -i -- "s|%SERVER1%|$1|g" /etc/nginx/conf.d/demo.conf
sudo service nginx restart
cd application
composer install
sudo service php8.0-fpm restart
sed -i -- "s|%DATABASE_HOST%|$2|g" .env
sed -i -- "s|%DATABASE_USER%|$3|g" .env
sed -i -- "s|%DATABASE_PASSWORD%|$4|g" .env
sed -i -- "s|%DATABASE_NAME%|$5|g" .env
php artisan migrate
sed -i -- "s|%RABBITMQ_HOST%|$6|g" .env
sed -i -- "s|%RABBITMQ_USER%|$7|g" .env
sed -i -- "s|%RABBITMQ_PASSWORD%|$8|g" .env
